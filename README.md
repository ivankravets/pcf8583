# PCF8583

PCF8583 Real Time Clock and Event Counter Library for Arduino

You can read about this library in my blog post about [Counting events with Arduino and PCF8583][1].

## Documentation

TBD :(

[1]: http://tinkerman.eldiariblau.net/counting-events-with-arduino-and-pcf8583
